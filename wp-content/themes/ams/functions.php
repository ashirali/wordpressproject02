<?php
require get_template_directory().'/inc/functions-admin.php';

//Adding CSS, JS and Bootstrap
function ams_enqueue_theme_scripts() {
    
  wp_enqueue_style( 'css', get_stylesheet_directory_uri() . '/assets/css/style.css');

  wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/assets/bootstrap-css/bootstrap.min.css');

  wp_enqueue_style( 'js', get_template_directory_uri() . '/assets/js/style.js');

  wp_enqueue_style( 'bootstarp-js', get_template_directory_uri() . '/assets/bootstrap-js/bootstrap.min.js');

  wp_enqueue_style('font-awesome5', get_template_directory_uri().'/assets/fontawesome/css/all.min.css');
}

add_action( 'wp_enqueue_scripts', 'ams_enqueue_theme_scripts');

//Adding Scripts
function ams_enqueue_scripts() {
  wp_enqueue_script( 'jquery');
}

add_action( 'wp_enqueue_scripts', 'ams_enqueue_scripts');

//Adding Admin CSS
function admin_css_style() {
  wp_enqueue_style('admin-css-styles', get_template_directory_uri().'/assets/css/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_css_style');

//Login Redirection

function my_login_redirect( $redirect_to, $request, $user ) {
  //Is there a user to check?
  if ( isset( $user->roles ) && is_array( $user->roles ) ) {
      //Check for admins
      if ( in_array( 'administrator', $user->roles ) ) {
          //Redirect them to the default place
          return $redirect_to;
      } else {
          return home_url();
      }
  } else {
      return $redirect_to;
  }
}

add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );
