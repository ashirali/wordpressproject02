<?php 
 if (!is_user_logged_in() ) { 
   wp_redirect( '/wordpress01/wp-login.php');
 }
 $user = wp_get_current_user();
get_header();?>
<style>
body {
  background: #f5f5f5;
}
.container {
    background-color: #f5f5f0;
}
.filter {   
    border-radius: 5px;
    width: 900px;
    padding: 5px;
}
.btn-outline-danger {   
    border-radius: 5px;
    margin-bottom: 7px;
}
</style>
<div class="container ">
<form action="">
<div class="form-group">
<select class="mdb-select md-form filter" name="month">
  <option>Select The Month</option>
  <?php
  for($m=1; $m<=12; ++$m) { 
  ?>
  <!--int mktime( $hour, $minute, $second, $month, $day, $year, $is_dst)-->
  <?php if( $m == isset($_GET['month'])) { ?>
    <option value="<?php echo $m; ?>"
    <?php
    if( $m == $_GET['month']) {
      echo "selected";
    }
    ?>
    
    ><?php echo date('F', mktime(0, 0, 0, $m, 1)); ?></option>
<?php
  } 
  else { ?>
    <option value="<?php echo $m; ?>"><?php echo date('F', mktime(0, 0, 0, $m, 1)); ?></option> 
  <?php  } 
  }
  ?>
</select>
<input class="btn btn-outline-danger" type="submit" name="submit" value="Get Selected Month"/>
<a href="http://localhost:81/wordpress01/" class="btn btn-outline-success">Clear</a>
</form>
</div>
<div class="container">
<table class="table text-center">
  <thead>
    <tr>
      <th>Date</th>
      <th>Day</th>
      <th>User Name</th>
      <th>Email</th>
      <th>Website</th>
      <th>Time In</th>
      <th>Time Out</th>
      <th>Total Time</th>
    </tr>
  </thead>
  <h1>
<?php
if ( ! in_array('administrator',$user->roles) ) :
  if (isset($_GET["submit"])){
    global $wpdb;
    $results=$wpdb->get_results("SELECT id,user_id, check_in, check_out, TIMEDIFF(check_out, check_in) AS diff FROM `ams_attendance` 
    WHERE MONTH(check_in) =".$_GET['month']." AND user_id = ".$user->ID);
  } else {
    $results = $wpdb->get_results("SELECT id,user_id, check_in, check_out, TIMEDIFF(check_out, check_in) AS diff FROM `ams_attendance` WHERE user_id =".$user->ID);
  }
    $hour = 0;
    $min = 0;
    $sec = 0;
    // foreach ($results as $row) {
      
    // }
    // if ($sec >= 60 OR $min >= 60) {
    //   $hour = $hour + intval($min/60);
    //   $min = ($min % 60);
    //   $min = $min + intval($sec/60);
    //   $sec = ($sec % 60);
    // }
  foreach ($results as $row) :
    $time = explode(':',$row->diff);
    $hour = $time[0];
    $min = $time[1];
    $sec = $time[2];
?>
    <tr>
      <td><?Php echo date('d/m/Y', strtotime( $row->check_in)); ?></td>
      <td><?Php echo date('l', strtotime( $row->check_in)) ; ?></td>
      <td><?Php echo $user->display_name ; ?></td>
      <td><?Php echo $user->user_email ; ?></td>
      <td><?Php echo $user->user_url; ?></td>
      <td><?Php echo date('h:i A', strtotime( $row->check_in)); ?></td>
      <td><?Php echo date('h:i A', strtotime( $row->check_out)); ?></td>
      <td><?php echo abs($hour).'h '.$min.'m '.$sec.'s '; ?></td>
      </tr>
      <?php
  endforeach;
 else :?>
  <tr><h1 class="text-center">Your are not allow to view the user's data.</h1></h1>
<?php endif;?>

  </tbody>
</table>
</div>
<?php get_footer();?>