<?php
wp_head();
/*
@package amstheme
    ========================
    ADMIN PAGE
    ========================

*/

function ams_add_admin_page() {
    //generate ams admin page
    add_menu_page('ams theme options', 'ams', 'manage_options', 'ashir_ams', 'ams_theme_create_page', get_template_directory_uri() . '/assets/images/icon1.png', 72);

    //generate ams admin sub pages
    add_submenu_page('ashir_ams', 'ams theme options', 'employees', 'manage_options', 'ashir_ams', 'ams_theme_create_page');
    //add_submenu_page('ashir_ams', 'ams css options', 'custom css', 'manage_options', 'ashir_ams_css', 'ams_theme_create_settings_subpage');
}
add_action('admin_menu', 'ams_add_admin_page');

function ams_theme_create_page() {
    //generation of page
    require_once( get_template_directory().'/inc/templates/ams-admin.php');
}

function ams_theme_create_settings_subpage() {
    //generation of setting subpage
    echo'<h1> Attendance Management System Custom CSS</h1>';
}
