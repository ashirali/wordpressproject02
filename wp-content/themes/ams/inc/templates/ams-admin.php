<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ams</title>
    <?php wp_head();?>
</head>
<body>
<style>
  .wp-toolbar.gr__localhost {
      padding-top: 0 !important;
  }
</style>
<h3 class="text-center mt-3">Attendance Management System Total Users</h3>
<h4 class="text-center mt-4 mb-4">Current Users</h4>
  <table class="table text-center">
  <thead>
    <tr>
      <th>User Name</th>
      <th>Email</th>
      <th>Website</th>
      <th colspan="2">Action</th>
    </tr>
  </thead>
  <tbody> 
<?php
$user_query = new WP_User_Query(array( 'role' => 'Subscriber' ) );
if ( ! empty( $user_query->get_results() ) ) {
  foreach ( $user_query->get_results() as $user ) {
?>
    <form action="<?php $_SERVER['PHP_SELF']; ?>" method="POST"> 
    <tr>
      <td><?Php echo $user->display_name; ?></td>
      <td><?Php echo $user->user_email; ?></td>
      <td><?Php echo $user->user_url; ?></td>
      <td colspan="2">
      <input type="hidden" name="user_id" class="btn btn-primary" value="<?php echo $user->ID; ?>">
      <input type="hidden" name="checkin" class="btn btn-primary" value="Check In">
      <input type="submit" name="checkinbtn" class="btn btn-primary" value="Check In">
      <input type="hidden" name="checkout" class="btn btn-success" value="Check Out">
      <input type="submit" name="checkoutbtn" class="btn btn-success" value="Check Out">
      </td>
    </tr> 
</form>
<?php
}
  } else {
	echo '<h1>No users found.</h1>';
    }
?>
</tbody>
</table>
</body>
<?php wp_footer();?>
</html>
<?php

if (isset($_POST["checkinbtn"])){
  global $wpdb;
  $currentTime = current_time('mysql', false);
  $row=$wpdb->get_row("SELECT * FROM ams_attendance WHERE user_id = ".$_POST['user_id']." and DATE(check_in) = CURDATE() ORDER BY id DESC");
  if($row){
    $wpdb->update('ams_attendance', array('check_out'=>$currentTime), array('id'=>$row->id));
  }else{
    $wpdb -> insert('ams_attendance', array ( 
      'check_in' => current_time('mysql', false),
      'user_id' =>  $_POST['user_id']
    ));
  }
}

if (isset($_POST["checkoutbtn"])){
  global $wpdb;
  $currentTime = current_time('mysql', false);
  $row=$wpdb->get_row("SELECT * FROM ams_attendance WHERE user_id = ".$_POST['user_id']." ORDER BY id DESC");
  $wpdb->update('ams_attendance', array('check_out'=>$currentTime), array('id'=>$row->id));
}
?>